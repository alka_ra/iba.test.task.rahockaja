package com.iba.testtask.rahockaja.resources.logger;

public enum LoggerChooser {
    FILE("FileLogger"), COMMAND("CommandLogger"), REG_KEY("RegKeyLogger");
    private String value;
    LoggerChooser(String value){
        this.value = value;
    }
    @Override
    public String toString(){
        return this.value;
    }
}
