package com.iba.testtask.rahockaja.commandimpl.cmdexecutor;

import com.iba.testtask.rahockaja.command.Command;
import com.iba.testtask.rahockaja.commandimpl.fileparser.utils.FileFactory;
import com.iba.testtask.rahockaja.resources.logger.LoggerChooser;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;

public class CmdExecutor extends Command {
    private static final Logger logger = Logger.getLogger(LoggerChooser.COMMAND.toString());

    private static final String FILE_OUTPUT_NAME = "cmd_out.txt";
    private static final String FILE_ERROR_OUTPUT_NAME = "cmd_err.txt";

    @Override
    public void execute() {

        File outFile;
        File errFile;
        try {
            outFile = FileFactory.getNewOrExistingFile(FILE_OUTPUT_NAME);
            errFile = FileFactory.getNewOrExistingFile(FILE_ERROR_OUTPUT_NAME);

            ProcessBuilder processBuilder = getProcessBuilder(getArgument(), outFile, errFile);

            runProcessFromABuilder(processBuilder);
        } catch (IOException e) {
            logger.info("Failed to create output files: " + e);
        }
    }

    private ProcessBuilder getProcessBuilder(String processName, File output, File err) {

        ProcessBuilder processBuilder = new ProcessBuilder(processName);

        processBuilder.redirectOutput(output);
        processBuilder.redirectError(err);

        return processBuilder;
    }

    private void runProcessFromABuilder(ProcessBuilder processBuilder) throws IOException {

        try {
            Process process = processBuilder.start();
            process.waitFor();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}