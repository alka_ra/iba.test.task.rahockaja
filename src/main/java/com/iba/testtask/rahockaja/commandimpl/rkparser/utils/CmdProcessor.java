package com.iba.testtask.rahockaja.commandimpl.rkparser.utils;

import java.io.*;

public class CmdProcessor {
    private static final String CMD = "cmd";

    public static String getInfoAboutRegisterKey(String path) throws IOException {
        Process processCmd = getProcess();
        BufferedReader bfIn = new BufferedReader(new InputStreamReader(processCmd.getInputStream()));
        BufferedWriter bfOut = new BufferedWriter(new OutputStreamWriter(processCmd.getOutputStream()));

        return searchRKWithCmd(bfIn, bfOut, path);
    }

    private static Process getProcess() throws IOException {
        Runtime rt = Runtime.getRuntime();
        return rt.exec(CMD);
    }

    private static String searchRKWithCmd(BufferedReader bfIn, BufferedWriter bfOut, String path) throws IOException {
        sendSearchCommandToCmd(bfOut, path);
        return receiveSearchResultFromCmd(bfIn, path);
    }

    private static void sendSearchCommandToCmd(BufferedWriter bfOut, String path) throws IOException {
        bfOut.write("reg query " + path + " /s");
        bfOut.newLine();
        bfOut.flush();
        if (bfOut != null) bfOut.close();
    }

    private static String receiveSearchResultFromCmd(BufferedReader bfIn, String path) throws IOException {
        int ir;
        StringBuilder string = new StringBuilder();
        while (null != bfIn) {
            if ((ir = bfIn.read()) != ':') {
                string.append((char) ir);
            } else {
                if (string.indexOf(path) == -1) {
                    string.append((char) ir);
                } else {
                    string.deleteCharAt(string.length() - 1);
                    bfIn.close();
                    bfIn = null;
                }
            }
        }
        return string.toString();
    }

}
