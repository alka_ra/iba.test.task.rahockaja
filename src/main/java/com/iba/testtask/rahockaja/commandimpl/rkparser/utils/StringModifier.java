package com.iba.testtask.rahockaja.commandimpl.rkparser.utils;

import java.util.ArrayList;
import java.util.List;

public class StringModifier {

    private static final String regexOutViewTarget = "[\t ]*(REG_[A-Z]+)[\t ]*";
    private static final String regexProperOutView = " = ";
    private static final String regexRegistryKey =
            "[^\n][\t ]*[A-Za-z0-9]+[\t ]+(REG_[A-Z]+)[\t ]+[A-Za-z0-9\\*\\#,]+[\\s]*";

    private static String modifyStringToOutView(String string) {
        return string.replaceAll(regexOutViewTarget, regexProperOutView);
    }

    public static List<String> getCmdResultInSuitableForm(CharSequence source) {
        List<String> stringsResult = new ArrayList();

        for (String string: source.toString().split("\n")) {
            if (string.matches(regexRegistryKey)) {
                string = StringModifier.modifyStringToOutView(string);
                stringsResult.add(string.trim()+"\n");
            }
        }
        return stringsResult;
    }
}