package com.iba.testtask.rahockaja.commandimpl.rkparser;

import com.iba.testtask.rahockaja.command.Command;
import com.iba.testtask.rahockaja.commandimpl.rkparser.utils.CmdProcessor;
import com.iba.testtask.rahockaja.commandimpl.rkparser.utils.StringModifier;
import com.iba.testtask.rahockaja.resources.logger.LoggerChooser;
import org.apache.log4j.Logger;

import java.io.*;
import java.util.List;

public class RegistryKeyParser  extends Command {
    private static final Logger logger = Logger.getLogger(LoggerChooser.REG_KEY.toString());

    private static final String FILE_OUTPUT_NAME = "rk_out.txt";

    @Override
    public void execute() {
        try {
            String path = getArgument();
            String resultSearchString = CmdProcessor.getInfoAboutRegisterKey(path);

            processTheOutput(resultSearchString, path);
        } catch (IOException e) {
            logger.info("Failed work with cmd: " + e);
        }
    }

    private static void processTheOutput(String unmodifiedData, String path) throws IOException {
        List<String> matchedStrings = StringModifier.getCmdResultInSuitableForm(unmodifiedData);
        if (matchedStrings.size() == 0) {
            logger.info("No registry keys are found for path :\"" + path + "\"");
        } else {
            writeInOutFile(matchedStrings);
        }
    }

    private static void writeInOutFile(List<String> strings) throws IOException {
        FileWriter fileWriter = new FileWriter(FILE_OUTPUT_NAME, true);
        for (String string : strings) {
            fileWriter.write(string);
        }
        fileWriter.close();
    }
}