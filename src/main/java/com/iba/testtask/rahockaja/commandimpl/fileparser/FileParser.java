package com.iba.testtask.rahockaja.commandimpl.fileparser;

import com.iba.testtask.rahockaja.command.Command;
import com.iba.testtask.rahockaja.commandimpl.fileparser.utils.FileFactory;
import com.iba.testtask.rahockaja.commandimpl.fileparser.utils.StringChecker;
import com.iba.testtask.rahockaja.commandimpl.fileparser.utils.StringModifier;
import com.iba.testtask.rahockaja.resources.logger.LoggerChooser;
import org.apache.log4j.Logger;

import java.io.*;

public class FileParser extends Command {
    private static final Logger logger = Logger.getLogger(LoggerChooser.FILE.toString());
    private static final String OUTPUT_FILE_NAME = "file_out.txt";

    @Override
    public void execute() {
        String inputFileName = StringModifier.getAbsolutePath(this.getArgument());

        File sourceFile;
        File outFile;
        try {
            sourceFile = FileFactory.getExistingFile(inputFileName);
            outFile = FileFactory.getNewOrExistingFile(OUTPUT_FILE_NAME);

            checkStreamsAndExchange(sourceFile, outFile);
        } catch (IOException e) {
            logger.info(e);
        }
    }

    private void checkStreamsAndExchange(File sourceFile, File destinationFile) throws IOException {
        PrintWriter out = null;
        BufferedReader in = null;
        try {
            in = new BufferedReader(new FileReader(sourceFile));
            out = new PrintWriter(destinationFile);

            parseAndWriteFileData(in, out);
        } catch (FileNotFoundException e) {
            logger.info("File does not exist: " + e);
        } finally {
            if (out != null) out.close();
            if (in != null) in.close();
        }
    }

    private void parseAndWriteFileData(BufferedReader in, PrintWriter out) throws IOException {
        String tmpString;

        while ((tmpString = in.readLine()) != null) {
            writeParsedStringToFile(out, tmpString);
        }
    }

    private void writeParsedStringToFile(PrintWriter out, String unCheckedString) {

        if (StringChecker.isStringGoodStructured(unCheckedString)) {

            String[] tmpKeyValueStrings = StringModifier.getKeyAndValueFromString(unCheckedString);

            for (String tmpString : tmpKeyValueStrings) {
                out.print(tmpString);
                out.print("\n");
            }
        } else {
            out.print(unCheckedString);
            out.print("\n");
        }
    }
}