package com.iba.testtask.rahockaja.commandimpl.fileparser.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

public class FileFactory {

    public static File getExistingFile(String fileName) throws FileNotFoundException {
        File file = new File(fileName);
        if (!file.exists()) {
            throw new FileNotFoundException("The file does not exist: " + fileName);
        }
        return file;
    }

    public static File getNewOrExistingFile(String fileName) throws IOException {
        File file = new File(fileName);
        if (!file.exists()) {
            file.createNewFile();
        }
        return file;
    }
}
