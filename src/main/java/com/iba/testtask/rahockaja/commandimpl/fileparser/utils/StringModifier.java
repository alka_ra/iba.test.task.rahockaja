package com.iba.testtask.rahockaja.commandimpl.fileparser.utils;

import java.io.File;

public class StringModifier {

    public static String getAbsolutePath(String receivedPath) {
        if (StringChecker.isThePathRelative(receivedPath)) {
            return makeAbsolutePathFromRelative(receivedPath);
        }
        return receivedPath;
    }

    private static String makeAbsolutePathFromRelative(String receivedPath) {
        if (File.separator.equals("\\")) {
            receivedPath = File.separator + receivedPath.replace("/", File.separator);
        } else{
            receivedPath = File.separator + receivedPath.replace("\\", File.separator);
        }

        return System.getProperty("user.dir") + receivedPath;
    }


    public static String[] getKeyAndValueFromString(String tmpStringStorage) {
        return tmpStringStorage.split("=", 2);
    }
}