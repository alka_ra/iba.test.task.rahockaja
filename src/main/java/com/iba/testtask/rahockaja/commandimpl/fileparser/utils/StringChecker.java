package com.iba.testtask.rahockaja.commandimpl.fileparser.utils;


public class StringChecker {

    private static final String regexpAbsolutePath = "^((/|\\|([A-Za-z]:(\\|/))).*)*";
    private static final String regexpKeyValue = "^([^=^\n]+=[^=^\n]+){1}";

    public static boolean isStringGoodStructured(String string) {
        return (string.matches(regexpKeyValue));
    }

    static boolean isThePathRelative(String string) {
        return string.matches(regexpAbsolutePath);
    }
}
