package com.iba.testtask.rahockaja;

import com.iba.testtask.rahockaja.command.Command;
import com.iba.testtask.rahockaja.command.utils.CommandChooser;

public class Main {
    public static void main(String[] args) {
        Command command = CommandChooser.chooseCommand(args);
        if (command != null) command.execute();
    }
}
