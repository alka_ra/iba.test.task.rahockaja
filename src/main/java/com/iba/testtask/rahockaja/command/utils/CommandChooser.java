package com.iba.testtask.rahockaja.command.utils;

import com.iba.testtask.rahockaja.command.Command;
import com.iba.testtask.rahockaja.commandimpl.cmdexecutor.CmdExecutor;
import com.iba.testtask.rahockaja.commandimpl.fileparser.FileParser;
import com.iba.testtask.rahockaja.commandimpl.rkparser.RegistryKeyParser;
import org.apache.log4j.Logger;

public class CommandChooser {

    private static final Logger logger = Logger.getLogger(CommandChooser.class);

    public static Command chooseCommand(String[] args) {
        Command command = null;
        try {
            checkArguments(args);
            CommandName commandName = getCommandName(args);
            command = getCommandFromName(commandName);
            command = setCommandProperties(command, args);
        } catch (IllegalArgumentException e){
            logger.info(e);
        }
        return command;
    }

    private static void checkArguments(String[] args) throws IllegalArgumentException{
        if (args.length < 1) {
            throw new IllegalArgumentException("No command detected.");
        }
    }

    private static CommandName getCommandName(String[] args) {
        return CommandName.fromValue(args[0]);
    }

    private static Command getCommandFromName(CommandName commandName) {
        switch (commandName) {
            case COMMAND:
                return new CmdExecutor();
            case FILE:
                return new FileParser();
            case REGISTRY_KEY:
                return new RegistryKeyParser();
            default:
                throw new IllegalArgumentException("Wrong command has been sent.");
        }
    }

    private static Command setCommandProperties(Command command, String[] args) {
        command.setArgument(args[1]);
        return command;
    }
}