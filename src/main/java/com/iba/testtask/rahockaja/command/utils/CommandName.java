package com.iba.testtask.rahockaja.command.utils;

public enum CommandName {

    FILE("-f"), COMMAND("-cmd"), REGISTRY_KEY("-rk");

    private final String value;

    CommandName(String value){
        this.value = value;
    }
    public static CommandName fromValue(String v) {
        for (CommandName command : CommandName.values()) {
            if (command.value.equals(v)) {
                return command;
            }
        }
        return null;
    }
}
