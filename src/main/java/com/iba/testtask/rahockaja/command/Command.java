package com.iba.testtask.rahockaja.command;

public abstract class Command {

    private String argument;
    public abstract void execute();

    public void setArgument(String argument){
        this.argument = argument;
    }
    protected String getArgument(){
        return this.argument;
    }
}
